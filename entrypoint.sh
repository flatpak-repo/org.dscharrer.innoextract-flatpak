#!/usr/bin/env bash
set -e

apt-get update
apt-get install -y flatpak flatpak-builder bzip2
apt-get install --reinstall -y ca-certificates
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install --noninteractive flathub org.freedesktop.Sdk//18.08 org.freedesktop.Platform//18.08
flatpak-builder --user --force-clean --install ./build ./org.dscharrer.Innoextract.yml
